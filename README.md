# LATTICE #



### What is LATTICE? ###

**LATTICE** (**L**aboratory Data **ATT**ached to **I**oT **C**onnected **E**nvironment) 
is a multi-band **IoT** network that focuses on experimental laboratory data extracted from scientific instruments or datasets.

**LATTICE** contains smart contracts that take external off-chain data and inserts it into a secure **IoT** blockchain 
based on **Chainlink**, **Polkadot**, **Tezos**, and others.

**LATTICE** has the ability to connect lab devices like microscopes and spectrometers 
via traditional broadband **TCP/IP**, **WiFi**, **XG** [**5G** and beyond], and **MQTT** 
depending on the amount of data and the use case.